# llPermission

Package for validating permissions with two levels

The first step to perform your tests is to create a class using the *trait* LlPermission\Permission.

File exPermission.php
```php
<?php
class exPermission {
    use \LlPermission\Permission;
}
```

The second step is to build the object with the user's permissions.

```php
$permissions = (new \LlPermission\PermissionSet())
    ->setRole('use');

$permissions->setClass(exPermission::class)
    ->setScope('read')
    ->setScopes(['create', 'delete']);
```

Finally, you can perform tests using the created class exPermission, which receives the created object as a parameter.

```php
$testPermission = new exPermission($permissions);

if (!$testPermission->create()) {
    throw new Exception('Unauthorized', 401);
}

echo 'Hello world!';
```

Please note that this translation assumes that the code provided is correct and functions as intended.