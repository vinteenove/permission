<?php
require_once __DIR__."/../vendor/autoload.php";

require_once __DIR__."/exPermission.php";


$permissions = (new \LlPermission\PermissionSet())
    ->setRole('use');

$permissions->setClass(exPermission::class)
    ->setScope('read')
    ->setScopes(['create','delete']);

$testPermission = new exPermission($permissions);

if(!$testPermission->create()){
    throw new Exception('Unauthorized',401);
}

echo 'Hello world!';