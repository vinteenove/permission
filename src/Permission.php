<?php

namespace LlPermission;


trait Permission{
    private PermissionInterface $permissions;

    public function __construct(PermissionInterface $permissions)
    {
        $this->permissions = $permissions;
    }

	/**
	 * @return bool
	 */
	public function isDev(): bool{
		return $this->permissions->getRole() === 'dev';
	}

    /**
     * @return bool
     */
    public function isAdmin(): bool{
        return $this->isDev() || $this->permissions->getRole() === 'admin';
    }

    /**
     * @return bool
     */
    public function isUser(): bool{
        return $this->isAdmin() ||  $this->permissions->getRole() === 'user';
    }


    protected function hasPermission(string $scope): bool{
        return $this->isDev() || $this->permissions->getPermissions(self::class, $scope);
    }


    public function __call(string $name, array $arguments): bool
    {
        return $this->hasPermission($name);
    }
}