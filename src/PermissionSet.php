<?php

namespace LlPermission;

class PermissionSet implements PermissionInterface
{
    private array $permissions;
    private string $role;

    public function setClass($class)
    {
        $this->permissions[$class] = [];

        return (new class ($this->permissions[$class]){
            private $permissions;

            public function __construct(&$permissions)
            {
                $this->permissions = &$permissions;
            }

            public function setScope($scope)
            {
                $this->permissions[$scope] = true;

                return $this;
            }

            public function setScopes(array $scopes)
            {
                $scopes = array_fill_keys($scopes, true);
                $this->permissions = array_merge($this->permissions, $scopes);

                return $this;
            }
        });
    }


    /**
     * @param $class
     * @param $scope
     * @return boolean
     */
    public function getPermissions($class, $scope)
    {
        return $this->permissions[$class][$scope] ?? false;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return $this
     */
    public function setRole(string $role)
    {
        $this->role = $role;

        return $this;
    }


}